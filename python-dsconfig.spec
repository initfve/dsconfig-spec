# Created by pyp2rpm-3.3.8
%global pypi_name dsconfig
%global pypi_version 1.6.4

Name:           python-%{pypi_name}
Version:        %{pypi_version}
Release:        1%{?dist}.maxlab
Summary:        Library and utilities for Tango device configuration

License:        GPLv3
URL:            https://gitlab.com/MaxIV/lib-maxiv-dsconfig
Source0:        %{pypi_source}
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3dist(setuptools)

%description
Dsconfig is a command line tool for managing configuration of Tango device
servers. It runs on python 2.7 as well as 3.6 and up.The goal of this project
is to provide tools for configuring a Tango database in a convenient way. Right
now the focus is on supporting Excel files as input ("xls2json"), but support
for other formats should follow.

%package -n     python3-%{pypi_name}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{pypi_name}}

Requires:       python3dist(jsonpatch) >= 1.13
Requires:       python3dist(jsonschema)
Requires:       python3dist(pytango)
Requires:       python3dist(six)
Requires:       python3dist(xlrd)
%description -n python3-%{pypi_name}
Dsconfig is a command line tool for managing configuration of Tango device
servers. It runs on python 2.7 as well as 3.6 and up.The goal of this project
is to provide tools for configuring a Tango database in a convenient way. Right
now the focus is on supporting Excel files as input ("xls2json"), but support
for other formats should follow.


%prep
%autosetup -n %{pypi_name}-%{pypi_version}
# Remove bundled egg-info
rm -rf %{pypi_name}.egg-info

%build
%py3_build

%install
%py3_install

%files -n python3-%{pypi_name}
%license LICENSE.txt
%doc README.md
%{_bindir}/json2tango
%{_bindir}/xls2json
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}-%{pypi_version}-py%{python3_version}.egg-info

%changelog
* Mon Jul 11 2022 Benjamin Bertrand - 1.6.4-1
- Fix classes properties
* Fri May 20 2022 Benjamin Bertrand - 1.6.2-1
- Minor fixes.
* Thu Mar 24 2022 Benjamin Bertrand - 1.6.0-1
- Initial package.
